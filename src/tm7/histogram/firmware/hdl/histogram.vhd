----------------------------------------------------------------------------------
-- Makes a histogram by counting occurrences and classifying them into bins
-- Alvaro Navarro, CIEMAT, 2018
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNIMACRO;
use UNIMACRO.vcomponents.all;

library work;
use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.bo2.all;

entity histogram is
  port(
    -- ipbus signals
    ipb_clk         : in std_logic;
    ipb_rst         : in std_logic;
    ipb_in          : in ipb_wbus;
    ipb_out         : out ipb_rbus;
    -- Input hit data
    clk             : in std_logic;
    rxData          : in std_logic_vector(83 downto 0)
  );
end entity;

architecture Behavioral of histogram is

  -- Signals for the ipbus register
  constant N_STAT : integer := 4;
  constant N_CTRL : integer := 4;
  signal ctrl_stb: std_logic_vector(N_CTRL-1 downto 0);
  signal stat : ipb_reg_v(N_STAT-1 downto 0);
  signal conf : ipb_reg_v(N_CTRL-1 downto 0);

  -- Signals for the block rams
  type addr_t is array(natural range <>) of std_logic_vector(8 downto 0);
  signal rdaddr, wraddr : addr_t(15 downto 0);
  signal wren : std_logic_vector(15 downto 0);
  signal di, do : ipb_reg_v(15 downto 0);
  

  --------------------------------------------------------------------------------
  -- Start of automatically-generated VHDL code for register "breakout" signals declaration
  signal conf_enable, conf_reset : std_logic := '0';

  signal stat_selected_bin_value : std_logic_vector(31 downto 0) := (others => '0');

  signal conf_select_bin : unsigned(4 downto 0) := (others => '0');
  signal conf_select_chan : unsigned(7 downto 0) := (others => '0');

  -- End of automatically-generated VHDL code for register "breakout" signals declaration
  --------------------------------------------------------------------------------


begin


  --------------------------------------------------------------------------------
  -- Start of automatically-generated VHDL code for register "breakout" signals assignment

  conf_reset                <= conf(16#00#)(0);
  conf_enable               <= conf(16#00#)(1);
  conf_select_bin           <= unsigned( conf(16#00#)(6 downto 2) );
  conf_select_chan          <= unsigned( conf(16#00#)(14 downto 7) );
  stat(16#00#)(31 downto 0) <= stat_selected_bin_value;

  -- End of automatically-generated VHDL code for register "breakout" signals assignment
  --------------------------------------------------------------------------------

  --------------------------------------
  -- IPbus register
  --------------------------------------
  reg: entity work.ipbus_syncreg_v
    generic map(N_CTRL => N_CTRL, N_STAT => N_STAT)
    port map(
      clk => ipb_clk,
      rst => ipb_rst,
      ipb_in => ipb_in,
      ipb_out => ipb_out,
      slv_clk => clk,
      d => stat,
      q => conf,
      --rstb => stat_stb,
      stb => ctrl_stb
    );
  
  stat_selected_bin_value <= do(to_integer(conf_select_chan(3 downto 0)));
  

  histogen : for ch in 15 downto 0 generate
  
  -----------------------------------------------------------------------
  --  READ_WIDTH | BRAM_SIZE | READ Depth  | RDADDR Width |            --
  -- WRITE_WIDTH |           | WRITE Depth | WRADDR Width |  WE Width  --
  -- ============|===========|=============|==============|============--
  --    37-72    |  "36Kb"   |      512    |     9-bit    |    8-bit   --
  --    19-36    |  "36Kb"   |     1024    |    10-bit    |    4-bit   --
  --    19-36    |  "18Kb"   |      512    |     9-bit    |    4-bit   --
  --    10-18    |  "36Kb"   |     2048    |    11-bit    |    2-bit   --
  --    10-18    |  "18Kb"   |     1024    |    10-bit    |    2-bit   --
  --     5-9     |  "36Kb"   |     4096    |    12-bit    |    1-bit   --
  --     5-9     |  "18Kb"   |     2048    |    11-bit    |    1-bit   --
  --     3-4     |  "36Kb"   |     8192    |    13-bit    |    1-bit   --
  --     3-4     |  "18Kb"   |     4096    |    12-bit    |    1-bit   --
  --       2     |  "36Kb"   |    16384    |    14-bit    |    1-bit   --
  --       2     |  "18Kb"   |     8192    |    13-bit    |    1-bit   --
  --       1     |  "36Kb"   |    32768    |    15-bit    |    1-bit   --
  --       1     |  "18Kb"   |    16384    |    14-bit    |    1-bit   --
  -----------------------------------------------------------------------

  begin
    BRAM_SDP_MACRO_inst : BRAM_SDP_MACRO
    generic map (
      BRAM_SIZE => "18Kb", -- Target BRAM, "18Kb" or "36Kb" 
      DEVICE => "7SERIES", -- Target device: "VIRTEX5", "VIRTEX6", "7SERIES", "SPARTAN6" 
      WRITE_WIDTH => 32,    -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
      READ_WIDTH => 32,     -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
      DO_REG => 0, -- Optional output register (0 or 1)
      INIT_FILE => "NONE",
      SIM_COLLISION_CHECK => "ALL", -- Collision check enable "ALL", "WARNING_ONLY", 
                          -- "GENERATE_X_ONLY" or "NONE"       
      WRITE_MODE => "READ_FIRST") -- Specify "READ_FIRST" for same clock or synchronous clocks
                          --  Specify "WRITE_FIRST for asynchrononous clocks on ports
      -- INIT, SRVAL, INIT_xx and INITP_xx declarations not specified ==> all zeroes
    port map (
      DO => do(ch),         -- Output read data port, width defined by READ_WIDTH parameter
      DI => di(ch),         -- Input write data port, width defined by WRITE_WIDTH parameter
      RDADDR => rdaddr(ch),   -- Input read address, width defined by read port depth
      RDCLK => clk,       -- 1-bit input read clock
      RDEN => '1',     -- 1-bit input read port enable
      REGCE => '1',       -- 1-bit input read output register enable
      RST => '0',         -- 1-bit input reset 
      WE => (3 downto 0 => '1'),-- Input write enable, width defined by write port depth
      WRADDR => wraddr(ch),   -- Input write address, width defined by write port depth
      WRCLK => clk,       -- 1-bit input write clock
      WREN => wren(ch));      -- 1-bit input write port enable


    process (clk)
      variable stored_addr : std_logic_vector(8 downto 0);
      variable cooldown : positive range 3 downto 0;
    begin
      if rising_edge(clk) then

        if conf_reset = '1' then -- Reset the histogram
          wren(ch) <= '1';
          di(ch) <= (others => '0');
          wraddr(ch) <= std_logic_vector(unsigned(wraddr(ch))+1);

        elsif conf_enable = '0' then -- if not counting, output the value selected by registers
          rdaddr(ch) <= std_logic_vector(conf_select_chan(7 downto 4) & conf_select_bin);
          wren(ch) <= '0';
          cooldown := 3;

        else -- update the histogram
          rdaddr(ch) <= rxData(83 downto 80) & rxData(4 + 5*ch downto 5*ch); 
          wraddr(ch) <= stored_addr;
          di(ch) <= std_logic_vector(    unsigned(do(ch))   +   bo2int(do(ch)/= (31 downto 0 => '1'))      );
          wren(ch) <= bo2sl(cooldown = 0);
          cooldown := cooldown - bo2int(cooldown>0);
          stored_addr := rdaddr(ch);

        end if;
      end if;
    end process;

  end generate;
  
end architecture;
