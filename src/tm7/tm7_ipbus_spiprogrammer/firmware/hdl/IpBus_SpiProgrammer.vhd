--------------------------------------------------------------------------------
-- Filename:              IpBus_SpiProgrammer.vhd
-- Date Created:          June 4, 2015
-- Version:               0.1

-- /v 0.1/ Just a test with no claim of performances.
--   The slave is not optimal. The write time to the SPI memory depends on the
--   ethernet latency, ~ O (100us) each reading/writing of individual register.
--   It can be optimazed introducing a FIFO with a depth equal to PAGE length of
--   the flash memory, which in the case of tmux is 256 bytes in total that is
--   64 words by 32 bits.
-- Legenda
--   SSD: SpiSerDes
--   SFP: SpiFlashProgrammer
-----------
--  Date --
-- 17.09.15  intSpiProgrammerSel polarity is now controlled by SFP_Status(35)
-- 17.09.15  Substituted 'regSFPReady_BusyB' with 'intSFPReady_BusyB'
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- TODO:
-- * rivedi commenti
-- * Rimuovere i signals e in/out dal 'MuxToSpiSerDes'  correlati al modulo
--   ´SpiFlashReader´ (SFR), non piu utilizzato
--------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;

library UNISIM;
use UNISIM.vcomponents.all;

use work.ipbus.all;
use work.IpBus_SpiProgrammer_data_types.all;
--use work.ipbus_reg_types.all;
--use work.ipbus_decode_IpBus_SpiProgrammer.all;
use work.ipbus_reg_types.all;

entity IpBus_SpiProgrammer is
	generic(
        FW_ID  :  std_logic_vector(31 downto 0) := x"00000000";
        EXTRA_PROTECTION: boolean := FALSE -- 0/FALSE: no extra protection on Programmer activation
	);
	port(
        ------------------------------------------------------------------------
        -- IpBus
		clk: in std_logic;   -- clock: in the following assumes 125/4 MHz
		rst: in std_logic;
		ipb_in: in ipb_wbus;
		ipb_out: out ipb_rbus;
        ------------------------------------------------------------------------
        -- SPI flash memory ports
        -- outSpiClk is output through STARTUPE2.USRCCLKO
        outSpiCsB           : out std_logic; -- Spi Chip Select. Connect to SPI Flash chip-select via FPGA FCS_B pin
        outSpiMosi          : out std_logic; -- Spi MOSI. Connect to SPI DQ0 pin via the FPGA D00_MOSI pin
        inSpiMiso           : in  std_logic; -- Spi MISO. Connect to SPI DQ1 pin via the FPGA D01_DIN pin
        outSpiWpB           : out std_logic; -- SPI flash write protect. Connect to SPI 'W_B/Vpp/DQ2'
        outSpiHoldB         : out std_logic  -- Connect to SPI 'HOLD_B/DQ3'. Fix to '1'.
        ------------------------------------------------------------------------
        -- Leds during programming phase
            -- leds: out std_logic_vector(7 downto 0) -- status LEDs
        ------------------------------------------------------------------------
    );
end IpBus_SpiProgrammer;

architecture rtl of IpBus_SpiProgrammer is
    ----------------------------------------------------------------------------
    --  SPI Programmer interface
    component SpiFlashProgrammer is
    port
    (
        inClk               : in  std_logic;
        inReset_EnableB     : in  std_logic;
        inCheckIdOnly       : in  std_logic;
        inVerifyOnly        : in  std_logic;
        inData32            : in  std_logic_vector(31 downto 0);
        inDataWriteEnable   : in  std_logic;
        outReady_BusyB      : out std_logic;
        outDone             : out std_logic;
        outError            : out std_logic;
        outErrorIdcode      : out std_logic;
        outErrorErase       : out std_logic;
        outErrorProgram     : out std_logic;
        outErrorTimeOut     : out std_logic;
        outErrorCrc         : out std_logic;
        outStarted          : out std_logic;
        outInitializeOK     : out std_logic;
        outCheckIdOK        : out std_logic;
        outEraseSwitchWordOK: out std_logic;
        outEraseOK          : out std_logic;
        outProgramOK        : out std_logic;
        outVerifyOK         : out std_logic;
        outProgramSwitchWordOK: out std_logic;

        -- Signals for SpiSerDes - Connect to instance of SpiSerDes
        outSSDReset_EnableB : out std_logic;
        outSSDStartTransfer : out std_logic;
        inSSDTransferDone   : in  std_logic;
        outSSDData8Send     : out std_logic_vector(7 downto 0);
        inSSDData8Receive   : in  std_logic_vector(7 downto 0)
    );
    end component SpiFlashProgrammer;

    component SpiSerDes is
    port
    (
        inClk           : in  std_logic;
        inReset_EnableB : in  std_logic;
        inStartTransfer : in  std_logic;
        outTransferDone : out std_logic;
        inData8Send     : in  std_logic_vector(7 downto 0);
        outData8Receive : out std_logic_vector(7 downto 0);
        outSpiCsB       : out std_logic;
        outSpiClk       : out std_logic;
        outSpiMosi      : out std_logic;
        inSpiMiso       : in  std_logic
    );
    end component SpiSerDes;
    ----------------------------------------------------------------------------
    -- IpBus interface
    component ipbus_ctrlreg_patched is
    generic(
    	ctrl_addr_width : natural := 0;
    	stat_addr_width : natural := 0
    );
    port(
    	clk: in std_logic;
    	reset: in std_logic;
    	ipbus_in: in ipb_wbus;
    	ipbus_out: out ipb_rbus;
    	d: in std_logic_vector(2 ** stat_addr_width * 32 - 1 downto 0);
    	q: out std_logic_vector(2 ** ctrl_addr_width * 32 - 1 downto 0)
    );
    end component ipbus_ctrlreg_patched;
    ----------------------------------------------------------------------------

    ----------------------------------------------------------------------------
    ----------------------------- Signals --------------------------------------
    ----------------------------------------------------------------------------
        --    --TODO: LEDs when reprogramming
        --    signal clk_led:         std_logic;
        --    signal cnt :            std_logic_vector(31 downto 0) := (others=>'0');
        --    signal LED_running:     std_logic_vector(3 downto 0)  := (0=>'1', others=>'0');
        --    signal LED_top, LED_bottom: std_logic_vector(3 downto 0):= (others=>'0');
        --    constant N_LED_STRECHED: integer:=1;
        --    signal led_q: std_logic_vector(N_LED_STRECHED-1 downto 0);
        --    -- Assumes IpBus clk fixed to 125/4=31.25 MHz. In any case not critical as it is for led blinking
        --    constant slv_Half_sec:  std_logic_vector(cnt'RANGE) :=std_logic_vector(to_unsigned(10#15_625_000#, cnt'LENGTH));

    signal ipbw: ipb_wbus_array(0 downto 0);
    signal ipbr: ipb_rbus_array(0 downto 0);

    signal intSSDSpiMosi,  intSSDSpiCsB: std_logic;	 
    ----------------------------------------------------------------------------

-- TODO: RIVEDERE (inizio)------------------------------------------------------------
-- alcuni dei seguenti signals sono da eliminare
  signal  intSFPReset_EnableB   : std_logic;
  signal  intSFPCheckIdOnly     : std_logic;
  signal  intSFPVerifyOnly      : std_logic;
  signal  intSFPData32          : std_logic_vector(31 downto 0);
  signal  intSFPDataWriteEnable : std_logic;
  signal  intSFPReady_BusyB     : std_logic;
  signal  intSFPDone            : std_logic;
  signal  intSFPError           : std_logic;
  signal  intSFPErrorIdcode     : std_logic;
  signal  intSFPErrorErase      : std_logic;
  signal  intSFPErrorProgram    : std_logic;
  signal  intSFPErrorTimeOut    : std_logic;
  signal  intSFPErrorCrc        : std_logic;
  signal  intSFPStarted           : std_logic;
  signal  intSFPInitializeOK      : std_logic;
  signal  intSFPCheckIdOK         : std_logic;
  signal  intSFPEraseSwitchWordOK : std_logic;
  signal  intSFPEraseOK           : std_logic;
  signal  intSFPProgramOK         : std_logic;
  signal  intSFPVerifyOK          : std_logic;
  signal  intSFPProgramSwitchWordOK : std_logic;

  -- 
  signal  intSpiProgrammerSel : std_logic;


--   SSD: SpiSerDes
--   SFR: SpiFlashReader (not used ?!)
  signal intSFRSSDReset_EnableB : std_logic;
  signal intSFRSSDStartTransfer : std_logic;
  signal intSFRSSDTransferDone  : std_logic;
  signal intSFRSSDData8Send     : std_logic_vector(7 downto 0);
  signal intSFRSSDData8Receive  : std_logic_vector(7 downto 0);


--   SSD: SpiSerDes
--   SFP: SpiFlashProgrammer
  signal intSFPSSDReset_EnableB : std_logic;
  signal intSFPSSDStartTransfer : std_logic;
  signal intSFPSSDTransferDone  : std_logic;
  signal intSFPSSDData8Send     : std_logic_vector(7 downto 0);
  signal intSFPSSDData8Receive  : std_logic_vector(7 downto 0);

  signal intSSDReset_EnableB    : std_logic;
  signal intSSDStartTransfer    : std_logic;
  signal intSSDTransferDone     : std_logic;
  signal intSSDData8Send        : std_logic_vector(7 downto 0);
  signal intSSDData8Receive     : std_logic_vector(7 downto 0);

--   SFR: SpiFlashReader (not used ?!)
  signal intSFRReset_EnableB    : std_logic;
  signal intSFRStartAddr32      : std_logic_vector(31 downto 0);
  signal intSFRWordCount32      : std_logic_vector(31 downto 0);
  signal intSFRData32           : std_logic_vector(31 downto 0);
  signal intSFRDataReady        : std_logic;
  signal intSFRDataAck          : std_logic;
  signal intSFRDone             : std_logic;
  signal intSpiClk              : std_logic;

  signal isysclk                : std_logic;
  signal sysclk                 : std_logic;
  signal intBlinker             : std_logic;
-- RIVEDERE (fine)--------------------------------------------------------------


--------------------------------------------------------------------------------
-- RIVEDERE (inizio)------------------------------------------------------------
  -- Constants
  constant  cStartTokenProgram: std_logic_vector(15 downto 0) := X"CA75";
  constant  cStartTokenCheckId: std_logic_vector(15 downto 0) := X"1DCD";
  constant  cStartTokenVerify : std_logic_vector(15 downto 0) := X"1423";

  -- Registers
  -- regSFPReset_EnableB: drives the SpiFlashProgrammer.inReset_EnableB input
  signal  regSFPReset_EnableB : std_logic                     := '1';

  -- Signals
  signal  intStartProgram     : std_logic;
  signal  intCheckIdOnly      : std_logic;
  signal  intVerifyOnly       : std_logic;


-- RIVEDERE (fine)--------------------------------------------------------------
---- copiato da 'JtagToSpiFlashProgrammer' -------------------------------------
--------------------------------------------------------------------------------

signal reg_statusSFP: std_logic_vector(31 downto 0) := X"0000_0000";
signal reg_ctrlSFP: std_logic_vector(63 downto 0)   := (36=>'1', others=>'0');

signal  rst_masked: std_logic;

begin

    rst_masked <= rst AND NOT intSpiProgrammerSel;

    ----------------------------------------------------------------------------
    --  SPI Programmer interface
    iSpiFlashProgrammer: SpiFlashProgrammer
    port map
    (
        inClk                 => clk,                     -- intBufgTck,
        inReset_EnableB       => intSFPReset_EnableB,
        inCheckIdOnly         => intSFPCheckIdOnly,
        inVerifyOnly          => intSFPVerifyOnly,
        inData32              => intSFPData32,
        inDataWriteEnable     => intSFPDataWriteEnable,
        outReady_BusyB        => intSFPReady_BusyB,
        outDone               => intSFPDone,
        outError              => intSFPError,
        outErrorIdcode        => intSFPErrorIdcode,
        outErrorErase         => intSFPErrorErase,
        outErrorProgram       => intSFPErrorProgram,
        outErrorTimeOut       => intSFPErrorTimeOut,
        outErrorCrc           => intSFPErrorCrc,
        outStarted            => intSFPStarted,
        outInitializeOK       => intSFPInitializeOK,
        outCheckIdOK          => intSFPCheckIdOK,
        outEraseSwitchWordOK  => intSFPEraseSwitchWordOK,
        outEraseOK            => intSFPEraseOK,
        outProgramOK          => intSFPProgramOK,
        outVerifyOK           => intSFPVerifyOK,
        outProgramSwitchWordOK=> intSFPProgramSwitchWordOK,

        -- Signals for SpiSerDes - Connect to instance of SpiSerDes
        outSSDReset_EnableB   => intSFPSSDReset_EnableB,
        outSSDStartTransfer   => intSFPSSDStartTransfer,
        inSSDTransferDone     => intSFPSSDTransferDone, --OK
        outSSDData8Send       => intSFPSSDData8Send,
        inSSDData8Receive     => intSFPSSDData8Receive
    );

    iSpiSerDes: SpiSerDes
    port map
    (
        inClk           => clk,                     -- intBufgTck,
        inReset_EnableB => intSSDReset_EnableB, -- TODO: sostituire con 'intSFPSSDReset_EnableB'
        inStartTransfer => intSSDStartTransfer, -- TODO: sostituire con 'intSFPSSDStartTransfer'
        outTransferDone => intSSDTransferDone,  -- TODO: sostituire con 'intSFPSSDTransferDone' --OK
        inData8Send     => intSSDData8Send,     -- TODO: sostituire con 'intSFPSSDData8Send'
        outData8Receive => intSSDData8Receive,  -- TODO: sostituire con 'intSFPSSDData8Receive'
        outSpiCsB       => intSSDSpiCsB,
        outSpiClk       => intSpiClk,
        outSpiMosi      => intSSDSpiMosi,
        inSpiMiso       => inSpiMiso
    );

-- connections
--   SSD: SpiSerDes
--   SFP: SpiFlashProgrammer
    intSSDReset_EnableB <= intSFPSSDReset_EnableB;
    intSSDStartTransfer <= intSFPSSDStartTransfer;

    intSFPSSDTransferDone <= intSSDTransferDone; --OK
    intSSDData8Send     <= intSFPSSDData8Send;
    intSFPSSDData8Receive <= intSSDData8Receive;

    STARTUPE2_inst : STARTUPE2
    port map (
        CLK => '0',
        GSR => '0',             -- 1-bit input: Global Set/rst input (GSR cannot be used for the port name)
        GTS => '0',             -- 1-bit input: Global 3-state input (GTS cannot be used for the port name)
        KEYCLEARB => '1',
        PACK => '1',            -- 1-bit input: PROGRAM acknowledge input
        USRCCLKO => intSpiClk,  -- 1-bit input: User CCLK input
        USRCCLKTS => '0',       -- must driven LOW to ensure the CCLK output is enabled
        USRDONEO => '1',
        USRDONETS => '1'
    );

    -- Parallel load of SpiFlashProgrammer status register
    reg_statusSFP(0)             <= intSFPReady_BusyB; -- [FM Sept 15] regSFPReady_BusyB;
    reg_statusSFP(1)             <= intSFPDone;
    reg_statusSFP(2)             <= intSFPError;
    reg_statusSFP(3)             <= intSFPErrorIdcode;
    reg_statusSFP(4)             <= intSFPErrorErase;
    reg_statusSFP(5)             <= intSFPErrorProgram;
    reg_statusSFP(6)             <= intSFPErrorTimeOut;
    reg_statusSFP(7)             <= intSFPErrorCrc;
    reg_statusSFP(8)             <= intSFPStarted;
    reg_statusSFP(9)             <= intSFPInitializeOK;
    reg_statusSFP(10)            <= intSFPCheckIdOK;
    reg_statusSFP(11)            <= intSFPEraseSwitchWordOK;
    reg_statusSFP(12)            <= intSFPEraseOK;
    reg_statusSFP(13)            <= intSFPProgramOK;
    reg_statusSFP(14)            <= intSFPVerifyOK;
    reg_statusSFP(15)            <= intSFPProgramSwitchWordOK;
    reg_statusSFP(31 downto 16)  <= X"0000";

    -- 32b data register (source data bus)
    intSFPData32  <=   reg_ctrlSFP (31 downto 0);
    -- Control regs via ipbus
--
    intSFPCheckIdOnly   <=   reg_ctrlSFP (32);
    intSFPVerifyOnly    <=   reg_ctrlSFP (33);
    intSFPDataWriteEnable <= reg_ctrlSFP (34);

    -- MASTER ENABLE BIT: must set to 1 in order to access to any function of the ....
    intSpiProgrammerSel <=   reg_ctrlSFP (35);



    ----------------------------------------------------------------------------
    --
    g_NO_EXTRA_PROTECTION: if not EXTRA_PROTECTION generate
        intSFPReset_EnableB <= not intSpiProgrammerSel;
    end generate g_NO_EXTRA_PROTECTION;
    --
    ----------------------------------------------------------------------------
    -- Extra protection from accidental reprogramming
    g_EXTRA_PROTECTION: if EXTRA_PROTECTION generate
        processWaitForStartToken : process (clk,intSpiProgrammerSel)
           begin
            if (intSpiProgrammerSel  = '0') then
                regSFPReset_EnableB <= '1'; -- Programmer disabled
            elsif (rising_edge(clk)) then
                if ((intStartProgram='1') or (intCheckIdOnly='1') or (intVerifyOnly='1')) then
                    regSFPReset_EnableB <= '0';
                end if;
            end if;
        end process processWaitForStartToken;

        intStartProgram <= to_std_logic(intSFPData32(15 downto 0) = cStartTokenProgram);
        intCheckIdOnly  <= to_std_logic(intSFPData32(15 downto 0) = cStartTokenCheckId);
        intVerifyOnly   <= to_std_logic(intSFPData32(15 downto 0) = cStartTokenVerify);

        intSFPReset_EnableB <=   regSFPReset_EnableB;
    end generate g_EXTRA_PROTECTION;
    --
    ----------------------------------------------------------------------------
    -- 
    ----------------------------------------------------------------------------
    -- TODO: LEDs when reprogramming
    ----------------------------------------------------------------------------
        --        clk_led <= clk; -- just an alias
        --        counter_proc: process(clk_led)
        --        variable ShiftDir: std_logic:='1';
        --        begin             
        --           if rising_edge(clk_led) then
        --                if cnt >= slv_Half_sec then
        --                    cnt <= (others=>'0');
        --                    if LED_running(LED_running'LEFT)='1' OR LED_running(LED_running'RIGHT)='1' then
        --                            ShiftDir := NOT ShiftDir;
        --                    end if;
        --                 if ShiftDir = '0' then
        --                        LED_running <= LED_running(LED_running'LEFT -1  DOWNTO LED_running'RIGHT) &
        --                                       LED_running(LED_running'LEFT);
        --                    else
        --                        LED_running <= LED_running(LED_running'RIGHT) &
        --                                       LED_running(LED_running'LEFT     DOWNTO LED_running'RIGHT+1);
        --                 end if;
        --                else
        --                    cnt <= std_logic_vector(unsigned(cnt) + 1);
        --                end if;
        --            end if;
        --        end process;
        --        LED_bottom  <=   LED_running when intSSDSpiCsB ='0' else FW_ID(3 downto 0);
        --        LED_top     <=  (
        --                not intSSDSpiCsB,  -- SWITHCHED ON when outSpiCsB (Spi Chip Select) is ASSERTED
        --                led_q(0),  -- pkt
        --                onehz,
        --                locked     -- TOP led
        --        );
        --        stretch: entity work.led_stretcher
        --            generic map(
        --                WIDTH => N_LED_STRECHED
        --            )
        --            port map(
        --                clk => clk125,
        --                d(0) => pkt,
        --                q => led_q
        --            );



    ----------------------------------------------------------------------------
    -- IpBus interface
    ipbusInterFace_inst: ipbus_ctrlreg_patched
    	generic map(
    		ctrl_addr_width => 1,
    		stat_addr_width => 0
    	)
    	port map(
                clk         => clk,
                reset       => rst_masked,
                ipbus_in    => ipb_in,
                ipbus_out   => ipb_out,
    		d           => reg_statusSFP,
                q           => reg_ctrlSFP 
    	);
    --


    ----------------------------------------------------------------------------
    -- Assign outputs
    outSpiWpB   <= intSFPCheckIdOK;
    outSpiHoldB <= '1';
    outSpiMosi  <= intSSDSpiMosi;
    outSpiCsB   <= intSSDSpiCsB;
    --TODO: leds        <= LED_bottom & LED_top;

end rtl;
