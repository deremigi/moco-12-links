##===================================================================================================##
##========================= Xilinx design constraints (XDC) information =============================##
##===================================================================================================##
##
## Company:               CERN
## Engineer:              Julian Mendez (julian.mendez@cern.ch)
##
## Project Name:          GBT-FPGA
##
## Target Device:         VC707 (Virtex 7 dev. kit)
## Tool version:          Vivado 2014.4
##
## Version:               4.0
##
## Description:
##
## Versions history:      DATE         VERSION   AUTHOR              DESCRIPTION
##
##                        17/12/2014   1.0       Julian Mendez       First .xdc definition
##
## Additional Comments:
##
##===================================================================================================##
##===================================================================================================##

##===================================================================================================##
##====================================  TIMING CLOSURE  =============================================##
##===================================================================================================##

##=================================================##
## MGT PCS to PMA rxslide constraint               ##
##=================================================##
##set_property RXSLIDE_MODE "PMA" [get_cells -hier -filter {NAME =~ */xlx_k7v7_mgt_ip/xlx_k7v7_mgt_ip_init/xlx_k7v7_mgt_ip_i}]
set_property RXSLIDE_MODE PMA [get_cells -hier -filter {NAME =~ *gbt_inst*gthe2_i}]







set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets rxFrameClk]
