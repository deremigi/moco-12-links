--============================================================================--
--#######################   Module Information   #############################--
--============================================================================--
--
-- Company          : CERN (EP-ESE-BE)
-- Engineer         : Julian Mendez <julian.mendez@cern.ch>
--
-- Project Name     : GBT-SC module (IC and EC fields)
-- Module Name      : IC_rx_fifo
--
-- Language         : VHDL
--
-- Target Device    : Device agnostic
-- Tool version     : -
--
-- Version          : 1.0
--
-- Description      : GBTx internal control - RX FIFO
--
-- Versions history : DATE      VERS.   AUTHOR      DESCRIPTION
--
--                    05/04/17  1.0     J. Mendez   First .vhd module definition
--
-- Add. Comments    : -
--                                                                              
--============================================================================--
--############################################################################--
--============================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--============================================================================--
--############################   Entity   ####################################--
--============================================================================--
entity ic_rx_fifo is
    generic (
        g_FIFO_DEPTH    : integer := 10;
        g_WORD_SIZE     : integer := 8
    );
    port (
        clk_i           : in  std_logic;
        reset_i         : in  std_logic;

        -- Data
        data_i          : in  std_logic_vector((g_WORD_SIZE-1) downto 0);
        data_o          : out std_logic_vector((g_WORD_SIZE-1) downto 0);

        -- Control
        new_word_i      : in  std_logic;
        write_i         : in  std_logic;
        read_i          : in  std_logic;
        
        --Status
        gbtx_addr_o     : out std_logic_vector(7 downto 0);
        mem_ptr_o       : out std_logic_vector(15 downto 0);
        nb_of_words_o   : out std_logic_vector(15 downto 0);
        
        to_be_read_o    : out std_logic        
    );
end ic_rx_fifo;

--============================================================================--
--#########################   Architecture   #################################-- 
--============================================================================--
architecture behavioral of ic_rx_fifo is

    -- Types
    subtype reg_t   is std_logic_vector((g_WORD_SIZE-1) downto 0);
    type ramreg_t   is array(integer range<>) of reg_t;

    -- Signals
    signal mem_arr              : ramreg_t(g_FIFO_DEPTH+7 downto 0);

    signal wr_ptr               : integer range 0 to g_FIFO_DEPTH+7;
    signal word_in_mem_size     : integer range 0 to g_FIFO_DEPTH+7;
    signal rd_ptr               : integer range 0 to g_FIFO_DEPTH+7;

    signal set_out              : std_logic := '0';    

begin                 --========####   Architecture Body   ####========-- 

    ram_proc: process(reset_i, clk_i)

    begin

        if reset_i = '1' then
            wr_ptr              <= 0;
            rd_ptr              <= 0;

            data_o              <= (others => '0');

            word_in_mem_size    <= 0;

        elsif rising_edge(clk_i) then

            -- Read
            if read_i = '1' and rd_ptr < word_in_mem_size then
                data_o          <= mem_arr(rd_ptr);
                rd_ptr          <= rd_ptr + 1;
            end if;

            if word_in_mem_size > rd_ptr then
                to_be_read_o    <= '1';
            else
                to_be_read_o    <= '0';
            end if;

            -- Write
            if write_i = '1' then
                mem_arr(wr_ptr) <= data_i;
                wr_ptr <= wr_ptr + 1;
            end if;
            
            if new_word_i = '1' then
                 
                if wr_ptr > 7 then
                    gbtx_addr_o             <= '0' & mem_arr(1)(7 downto 1);
                    nb_of_words_o           <= mem_arr(4) & mem_arr(3);
                    mem_ptr_o               <= mem_arr(6) & mem_arr(5);
                    word_in_mem_size        <= wr_ptr-1;
                    rd_ptr                  <= 7;

                else
                    word_in_mem_size        <= 0;
                    rd_ptr                  <= 0;

                end if;

                wr_ptr <= 0;

            end if;
            
        end if;

    end process;

end behavioral;
--============================================================================--
--############################################################################--
--============================================================================--
